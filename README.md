## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем


# Лабораторна робота №2<br>"Розширена робота з git"

## КВ-11 Чебан Максим

## Хід виконання роботи

### 1. Зклонувати репозиторій для цієї роботи використавши локальний репозиторій від lab1 в якості "віддаленого".
Клонуємо репозиторій для цієї роботи , використовуючи абсолютний шлях до локальної
папки з репозиторієм від lab1. У мене це шлях: file:///home/student/Lab1/hydrogen
Так як git -- розподілена система контролю версій, кожна локальна копія може трактуватися як
повноцінний репозиторій, тому для нашого клону lab2 -- локальна копія lab1 виглядає точно таким же
"віддаленим сервером", як власне посиланя на github виглядало для lab1.

```
student@virt-linux:~/lab2$ git clone file:///home/student/Lab1/hydrogen
Cloning into 'hydrogen'...
remote: Enumerating objects: 60698, done.
remote: Counting objects: 100% (60698/60698), done.
remote: Compressing objects: 100% (14860/14860), done.
remote: Total 60698 (delta 44228), reused 60621 (delta 44182)
Receiving objects: 100% (60698/60698), 41.95 MiB | 15.24 MiB/s, done.
Resolving deltas: 100% (44228/44228), done.

```
Як бачимо репозиторій зклонувався успішно.
Перевіримо за допомогою `git branch`, що в копії репозиторію lab2 у нас присутні 
гілки, які ми створювали для lab1:
```
student@virt-linux:~/lab2/hydrogen$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/lab2-branch
  remotes/origin/master

```
Як можна помітити, всі створенні гілки присутні, отже лонування пройшло правильно.
### 2. Робота з ремоутами
Перевіримо куди "дивиться" наш поточний ремоут origin:
```
student@virt-linux:~/lab2/hydrogen$ git remote -v
origin	file:///home/student/Lab1/hydrogen (fetch)
origin	file:///home/student/Lab1/hydrogen (push)

```
# 2.1 Додати новий ремоут використавши URI на інтернет-джерело вашого репозиторію.
Інтернет репозиторій обраний в першій лабораторній роботі має посилання:
https://github.com/hydrogen-music/hydrogen.git
Додамо цей новий ремоут, використавше вказане інтернет-джерело взятого репозиторію.
```
student@virt-linux:~/lab2/hydrogen$ git remote add upstream https://github.com/hydrogen-music/hydrogen.git

```
Перевіримо наявність нового ремоуту
```
student@virt-linux:~/lab2/hydrogen$ git remote -v
origin	file:///home/student/Lab1/hydrogen (fetch)
origin	file:///home/student/Lab1/hydrogen (push)
upstream	https://github.com/hydrogen-music/hydrogen.git (fetch)
upstream	https://github.com/hydrogen-music/hydrogen.git (push)

```
Бачимо, що додався новий ремоут, якого раніше не було.
# 2.2 Показати список віддалених гілок, так щоби було видно гілки з різних ремоутів.
Спочатку за допомогою функції `git fetch upstream` ми завантажуємо нові гілки, які додані були ремоутом.
Тепер покажемо, що ми додали новим ремоутом, тим самим транслюючи нові гілки з нового ремоуту
та старі гілки з початкового ремоуту:
```
student@virt-linux:~/lab2/hydrogen$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/lab2-branch
  remotes/origin/master
  remotes/upstream/0.9.4
  remotes/upstream/0.9.5
  remotes/upstream/0.9.6
  remotes/upstream/0.9.7
  remotes/upstream/1.0.0
  remotes/upstream/4a9ae7ea-artifacts
  remotes/upstream/French-translation
  remotes/upstream/H2asLV2
  remotes/upstream/allSanitizers
  remotes/upstream/appveyor-build-with-artifacts
  remotes/upstream/appveyor-osx-compat
  remotes/upstream/coverity-scan
  remotes/upstream/dev-audio-engine-timing
  remotes/upstream/dev-screenshot-widgets
  remotes/upstream/dev-tooltips
  remotes/upstream/develop
  remotes/upstream/development
  remotes/upstream/drumkit-cli-1.1.1
  remotes/upstream/extEditor_26

```
Можна також відзначити, що шлях `remotes/origin` і `remotes/upstream` вказує, якому з ремоутів належить гілка.
# 2.3 Створення нової гілки і додавання до неї декількох комітів.
Створення нової гілки
```
student@virt-linux:~/lab2/hydrogen$ git checkout -b lab2-branch
Switched to a new branch 'lab2-branch'

```
Тепер створимо на ній два нових комітів, спочатку додавши декілька файлів, наприклад `main.cpp`,`textfile.txt`,`file1.h`
і будемо працювати з створеними файлами.
```
student@virt-linux:~/lab2/hydrogen$ git status
On branch lab2-branch
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	file1.h
	main.cpp
	textfile.txt

nothing added to commit but untracked files present (use "git add" to track)


student@virt-linux:~/lab2/hydrogen$ git add file1.h
student@virt-linux:~/lab2/hydrogen$ git commit -m "1 file was added"
[lab2-branch 4d3493899] 1 file was added
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 file1.h


student@virt-linux:~/lab2/hydrogen$ git add .
student@virt-linux:~/lab2/hydrogen$ git commit -m "all files were added" 
[lab2-branch a45bbb160] all files were added
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 main.cpp
 create mode 100644 textfile.txt


student@virt-linux:~/lab2/hydrogen$ git status
On branch lab2-branch
nothing to commit, working tree clean

```
Отже, гілка створена, коміти зроблені.
# 2.4 Пушнути створену гілку `lab2-branch` на ремоут з lab1 без зв'язувань локальної гілки з віддаленою
Якщо просто написфти `git push`, то буде виведена помилка, оскільки гіт не хоче брати на себе відповідальність
обирати, на яку саме віддалену копію репозиторію ми хочемо покласти нову гілку.
```
student@virt-linux:~/lab2/hydrogen$ git push
fatal: The current branch lab2-branch has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin lab2-branch


```

Отже, спробуємо запушити створену гілку вказуючи явно назву віддаленої копії.
```
student@virt-linux:~/lab2/hydrogen$ git push -u origin lab2-branch
Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
Delta compression using up to 2 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 716 bytes | 716.00 KiB/s, done.
Total 6 (delta 3), reused 0 (delta 0)
To file:///home/student/Lab1/hydrogen
   1912a4ed4..79258e3e0  lab2-branch -> lab2-branch
Branch 'lab2-branch' set up to track remote branch 'lab2-branch' from 'origin'.

```
Як бачимо відповідний пуш вдалий.
Спробуємо знову запушити без вказування віддаленої копії і запевнимось, що зв'язування гілок не відбулось.
В такому випадку при `git push` знову видаватиметься помилка.
```
$ git push
fatal: The current branch lab2-branch has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin lab2-branch
```
Помилка видається, отже гілка пушнута без зв'язування.
# 2.5 Додати ще коміт.
Додамо ще один довільний коміт. Наприклад, видалимо один з доданих файлів раніше і закомітимо цю змінну.
```
student@virt-linux:~/lab2/hydrogen$ git rm main.cpp
rm 'main.cpp'
student@virt-linux:~/lab2/hydrogen$ git commit -m "deleted file main.cpp"
[lab2-branch 55fe96ace] deleted file main.cpp
 1 file changed, 0 insertions(+), 0 deletions(-)
 delete mode 100644 main.cpp

```
# 2.6 Пушнути створену гілку `lab2_my_own_branch` на ремоут з lab1 зв'язавши гілки.
Після цього пушу подальші уже можна виконувати без параметрів, оскільки локальна гілка зв'язалась 
з конкретною віддаленою.
```
student@virt-linux:~/lab2/hydrogen$ git push -u origin lab2-branch
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Delta compression using up to 2 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 235 bytes | 235.00 KiB/s, done.
Total 2 (delta 1), reused 0 (delta 0)
To file:///home/student/Lab1/hydrogen
   79258e3e0..55fe96ace  lab2-branch -> lab2-branch
Branch 'lab2-branch' set up to track remote branch 'lab2-branch' from 'origin'.

student@virt-linux:~/lab2/hydrogen$ git push
Everything up-to-date

```
Після зв'язування бачимо, що помилки не видаються, пуш проведено успішно.
Пушіння без зв'язування і з відрізняється лише наявністю ключа -u, який і проводить зв'язку. 
# 2.7 Додамо ще коміт
Додамо ще коміт для наступного пушу, щоб перевірити точно, що зв'язування гілок відбулось.
```
student@virt-linux:~/lab2/hydrogen$ git mv file1.h f1.h

student@virt-linux:~/lab2/hydrogen$  git commit -m "renamed file1.h to f1.h"
[lab2-branch 06f34c1b3] renamed file1.h to f1.h
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename file1.h => f1.h (100%)

```
# 2.8 Переконатися, що після зв'язування гілок просто можна пушити через `git push`.
```
student@virt-linux:~/lab2/hydrogen$ git push
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Delta compression using up to 2 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (2/2), 247 bytes | 247.00 KiB/s, done.
Total 2 (delta 1), reused 0 (delta 0)
To file:///home/student/Lab1/hydrogen
   55fe96ace..06f34c1b3  lab2-branch -> lab2-branch

```
Як бачимо git push був вдачним, все працює.
# 2.9 Перевіримо наявність нової локальної гілки в репозиторії lab1.
Оскільки віддалені гілки не є транзитивними, lab2 бачить "віддалені гілки" з lab1 лише ті, що в lab1 є локальними.
Тобто git push, що був виконаний при зв'язувані гілок, створив в lab1 нову локальну гілку `lab2_my_own_branch`.
```
student@virt-linux:~/Lab1/folder$ git branch
  lab2-branch
  my_branch_1
  my_branch_2
  my_branch_3
* my_branch_5
  mybranch_4
  releases/1.2

```
### 3. Змерджити гілку, що була створена при виконанні lab1, в поточну гілку lab2_my_own_branch.
Об'єднаємо стани двох гілок:
* створеної в цій роботі
* створеної в роботі lab1

Перевіримо поточний стан гілок і на якій гілці ми зараз знаходимось:
```
$ git branch -a
* lab2-branch
  remotes/origin/HEAD -> origin/i18n
  remotes/origin/i18n
  remotes/origin/lab2_my_own_branch
  remotes/origin/my_branch_1
  remotes/origin/my_branch_2
  remotes/origin/my_branch_3
  remotes/origin/my_branch_5
  remotes/origin/mybranch_4
  remotes/origin/releases/1.2
  remotes/upstream/astro
  remotes/upstream/axios
  remotes/upstream/date-fns
  remotes/upstream/debiman
  remotes/upstream/decaffeinate
  remotes/upstream/fix-manifest-db_size
  remotes/upstream/fix-options
  remotes/upstream/flask
  remotes/upstream/i18n
  remotes/upstream/improve-python
  remotes/upstream/main
  remotes/upstream/mdn-fix-bcd
  remotes/upstream/numpy
  remotes/upstream/openjdk
  remotes/upstream/python
  remotes/upstream/renovate/image_optim_pack-0.x-lockfile
  remotes/upstream/renovate/minitest-5.x-lockfile
  remotes/upstream/renovate/rss-0.x-lockfile
  remotes/upstream/renovate/ruby-on-rails-packages
  remotes/upstream/requests
  remotes/upstream/ruby_3.1.2
  remotes/upstream/sequelize
  remotes/upstream/sphinx-allow-dt-styles
  remotes/upstream/svelte
  remotes/upstream/vitest
```

Бачимо, що локальної копії гілки lab1 у нас наразі немає. Але це потрібно для їх об'єднання. Тож використаємо
явно адресу коміту з віддаленої гілки.

```
$ git merge origin/my_branch_1
Merge made by the 'recursive' strategy.
 file.txt | 0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 6.txt

```
Об'єднання цих двох гілок відбулось за допомогою `git log` виводячи граф комітів в якийсь момент буде видно, що 
гілки розійшлись, буде відмічено дві голови, а також останній - об'єднуючий мердж-коміт.
```
$ git log --pretty=oneline --graph
*   b49c524c9c37c493beb7f613f2b236ebd8707748 (HEAD -> lab2_my_own_branch) Merge remote-tracking branch 'origin/my_own_branch_1' into lab2_my_own_branch
|\  
| * 8801f3150be01782aad25219aa089088ae6f5733 (origin/my_own_branch_1) add 6.txt
* | 616c3e4fd9f4cbdbeaa41ac5acbf6207401e81e7 (origin/lab2_my_own_branch) rename ola.h
* | edf6057057cf9f3e5b6559e24644d64f4e1bedcd deleted main.cpp
* | 5f7f806406bd0d85653f0e16c2eb4410682c0758 all file
* | e84ef90aac00c138c18ef31752a84cb3e6c1b8a8 one file added
|/  
* d2d6cd646bdf80e5f57c389b4538ff514fec11d4 (origin/my_own_branch_3, origin/i18n, origin/HEAD, i18n) rename 1.txt to file1.txt
* e26dccd50afb88c080a37485615559f11b5a5bca delete inc.h
* 1f3f36365c6645ecda1c2661bc2f0741781ea0fc add all chenges
* cafab666e66956920746929cbe2598230305af29 add 1.txt
* 510f9f19f44cf4393f0df81121387ee958eb3960 (grafted, upstream/i18n) Add __ helper for common phrases

```
### 4. Перенесення комітів.
# 4.1 Створити ще одну гілку і додати до неї три коміти.
Було створено нову гілку `lab2_branch2`, а також декілька файлів. Були внесені зміни до них, були зроблені коміти.
# 4.2 Перенести з гілки lab2_my_own_branch_2 середній з трьох нових комітів в гілку lab2_my_own_branch.
Так, як нам потрібно перенести саме середній з трьох зроблених комітів, виконаємо `git log` та знайдемо хеш
бажаного коміту.
```
student@virt-linux:~/lab2/hydrogen$ git log
commit f109e1ef7fa28613ae810b558ebd2daa6e82f20d (HEAD -> lab2-branch2)
Author: Cheban Maksym <md.cheban@gmail.com>
Date:   Tue Dec 26 01:51:28 2023 +0200

    new.cs was added

commit 111614ec3cb00062d528198b760be112dffcc0fc
Author: Cheban Maksym <md.cheban@gmail.com>
Date:   Tue Dec 26 01:50:52 2023 +0200

    file.cs

commit 97e4cfc1ee3d5aeaaf51f26584db5cae39d7a41b
Author: Cheban Maksym <md.cheban@gmail.com>
Date:   Tue Dec 26 01:50:33 2023 +0200

    fff.txt added

commit dbc22d507047fdc0f325db3447e95ee1a08bc656 (lab2-branch)
Merge: 06f34c1b3 2f95c047f
Author: Cheban Maksym <md.cheban@gmail.com>
Date:   Tue Dec 26 01:48:11 2023 +0200

```
Бачимо, що відповідний шуканий для нас хеш `111614ec3cb00062d528198b760be112dffcc0fc`.
Тоді, переключаємось на гілку, на яку переноситимемо коміт.
```
student@virt-linux:~/lab2/hydrogen$ git checkout lab2-branch
Switched to branch 'lab2-branch'
Your branch is ahead of 'origin/lab2-branch' by 7 commits.
  (use "git push" to publish your local commits)


student@virt-linux:~/lab2/hydrogen$ git branch
* lab2-branch
  lab2-branch2
  master

```

Після переключення виконуємо перенесення коміту за допомогою `git cherry-pick`

```
student@virt-linux:~/lab2/hydrogen$ git cherry-pick 111614ec3cb00062d528198b760be112dffcc0fc
[lab2-branch d44961b44] file.cs
 Date: Tue Dec 26 01:50:52 2023 +0200
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 file.cs

```
Як можна помітити, перенесення коміту відбулось успішно.
Продемонструємо за допомогою `git log --pretty=oneline --graph -n 10 --branches` факт того, що
коміт перенесено, зазначимо, що параметр `--branches` вказує на те, що показуються коміти не 
лише явно назад по історії поточної гілки, а всі локальні гілки, в тому числі і паралельні.

```
student@virt-linux:~/lab2/hydrogen$ git log --pretty=oneline --graph -n 10 --branches
* d44961b44f462944d7b6707d7e7628de3f467e39 (HEAD -> lab2-branch) file.cs
| * f109e1ef7fa28613ae810b558ebd2daa6e82f20d (lab2-branch2) new.cs was added
| * 111614ec3cb00062d528198b760be112dffcc0fc file.cs
| * 97e4cfc1ee3d5aeaaf51f26584db5cae39d7a41b fff.txt added
|/  
*   dbc22d507047fdc0f325db3447e95ee1a08bc656 1
|\  
| *   2f95c047f73452386def1760724ef9ffc7b0d7f6 (upstream/develop) Merge pull request #1685 from theGreatWhiteShark/phil-check-license-notice
| |\  
| | * e178b2cea346ba19b5e211702df9cb138db4b46a appVeyor: fail pipeline on failing license check
| | * 102383c0398f2d5ba0f4800d392030a6e02a88cd license check: fix typo
| | * 94ae57934fd0faf2949243a0523e12d324bf91ce automated license checking
| | * b35c013bda50af96e38e5e0df2f5ed2252980c7e PreferencesDialog: fix type in license notice

```
### 5. Визначити останнього спільного предка між двома будь-якими гілками.
Так, як часто буває, що потрібно визначити в який саме момент була створена гілка.
Незначно, якщо з моменту гілкування від базової гілки пройшло лише декілька комітів, але якщо в гілці дуже
багато комітів, оптимальним методом є команда `git merge-base`.

Визначимо момент, коли гілки `lab2_branch` і `my_branch_2` "розійшлися".
```
student@virt-linux:~/lab2/hydrogen$ git merge-base lab2-branch lab2-branch2
dbc22d507047fdc0f325db3447e95ee1a08bc656

```
Як бачимо, результат виконання є лише один хеш коміту, але це і є потрібний момент, коли гілки розійшлися.
Цей же коміт ще називають "базовим", оскільки саме відносно нього відбувається вирішення конфліктів. 
Взагалі конфлікт виникає саме в той момент, коли дві паралельні гілки зробили зміни до одного і того ж місця, 
після чого дані зміни намагаються об'єднатися.

Часто трапляється таке, що є багато незакомічених файлів у гілці, але нам потрібно терміново перемкнутись на іншу
і зробити в іншій гілці, якесь завдання, а потім знову повернутись до цієї гілки, тоді, щоб це все не комітити 
ми можемо виконати один з двох способів:
* Зробити нову гілку і зміни зберегти в ній.
* Якщо змін не багато, можна використати ничку. 
# 6.1 Зробити трохи unstaged змін.
Модифікуємо деякі файли.
```
student@virt-linux:~/lab2/hydrogen$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 8 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   file.cs
	modified:   treestate.py

no changes added to commit (use "git add" and/or "git commit -a")

```
Бачимо, що було модифіковано два файли `file.cs`, `treestate.py`
# 6.2 Зберегти до нички.
Збережемо зміни з попереднього пункту до нички.
```
student@virt-linux:~/lab2/hydrogen$ git stash
Saved working directory and index state WIP on lab2-branch: d44961b44 file.cs

student@virt-linux:~/lab2/hydrogen$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 8 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean

```
Отримали чисте робоче дерево.
Переглянемо вміст нички:
```
student@virt-linux:~/lab2/hydrogen$ git stash list
stash@{0}: WIP on lab2-branch: d44961b44 file.cs

```
# 6.3 Зробити ще трохи unstaged змін.
Модифікуємо ще деякі файли.
```
student@virt-linux:~/lab2/hydrogen$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 8 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   file.js
	modified:   textfile.txt
	modified:   treestate.py

no changes added to commit (use "git add" and/or "git commit -a")

```
Бачимо, що було модифіковано два файли `file.js`, `textfile.txt`, `treestate.py`
# 6.4 Зберегти до нички.
Збережемо зміни з попереднього пункту до нички.
```
student@virt-linux:~/lab2/hydrogen$ git stash
Saved working directory and index state WIP on lab2-branch: d44961b44 file.cs


student@virt-linux:~/lab2/hydrogen$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 8 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean

```
Отримали чисте робоче дерево.
Переглянемо вміст нички:
```
student@virt-linux:~/lab2/hydrogen$ git stash list
stash@{0}: WIP on lab2-branch: d44961b44 file.cs
stash@{1}: WIP on lab2-branch: d44961b44 file.cs

```
# 6.5 Дістанемо з нички перші збережені зміни з пункту 6.2.
Щоб дістати файли з нички є два варіанти:
* `git stash pop` - він застосує і видалить останній заниканий стан
* `git stash apply` - застосує останній збережений стан, але не видалить його з нички.
Другий варіант корисний якщо є щось, що не можна комітити, але постійно треба переносити з 
гілки на гілку і мати в робочому дереві.

Якщо у нас декілька ничок, то для їх застосування можна передавати повну назву, щоби застосувати до
поточного робочого дерева саме конкретний збережений стан, а не останній.

Отже, дістанемо ничку з пункту 6.2 за допомогою першого варіанту з видаленням нички.
```
student@virt-linux:~/lab2/hydrogen$ git stash pop stash@{1}
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 8 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   file.cs
	modified:   treestate.py

no changes added to commit (use "git add" and/or "git commit -a")
Dropped stash@{1} (8fb63690e5b9324857753849663a9e131c535a0d)

```
Як бачимо з нички дістались два модифікованні файли, які були сховані при першій ничці у пункті 6.2
Так як при введені `git stash list` ми бачимо лише одну ничку, то це означає що `git stash pop` успішно видалила 
ничку з пункту 6.2
```
student@virt-linux:~/lab2/hydrogen$ git stash list
stash@{0}: WIP on lab2-branch: d44961b44 file.cs

```
### 7. Робота з файлом .gitignore.
# 7.1 Створити кілька файлів з якимось унікальним розширенням.
Унікальне розширення це те, яке ще жодного разу не застосовувалось.
Я створив три файли з унікальним розширенням `file1.mawsadw123`, `file2.mawsadw123`, `file3.mawsadw123`, а
також файл `texty.txt`.
# 7.2 Додати шаблон для цих файлів в ігнор.
Часто у нас є в репозиторії скрипти збірки - тестів - що генерують файли, які не потрібно комітити.
Але ці файли завжди підсвічуватимуться в статусі, що може відволікати від вагомих змін, тож щоб випадково
не помилитись і постійно не відволікатись на ці файли існує файл `.gitignore`. Його ім'я починається з крапки, 
що в Лінуксі позначає прихований файл.

Створюємо за допомогою `touch` в корені репозиторію файл з назвою `.gitignore` і додаємо до
нього наступний рядок:
```
*.mawsadw123
```
Таким чином, ми додали шаблон непотрібних для бачення файлів в ігнор.
# 7.3 Перевірити статус -- файли повинні зникнути.
```
student@virt-linux:~/lab2/hydrogen$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 8 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   .gitignore
	modified:   file.cs
	modified:   treestate.py

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	texty.txt

no changes added to commit (use "git add" and/or "git commit -a")


no changes added to commit (use "git add" and/or "git commit -a")
```
Як бачимо в статусі більше файли з розширенням `.mawsadw123` не відображаються.
# 7.4 Перевірити статус включно з ігнором.
Перевірка репозиторію на наявність ігнорованих файлів використовується ключ `--ignored`.
```
student@virt-linux:~/lab2/hydrogen$ git status --ignored
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 8 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   .gitignore
	modified:   file.cs
	modified:   treestate.py

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	texty.txt

Ignored files:
  (use "git add -f <file>..." to include in what will be committed)
	file1.mawsadw123
	file2.mawsadw123
	file3.mawsadw123

no changes added to commit (use "git add" and/or "git commit -a")

```
Як можна помітити, тепер видно всі незбереженні та незакомічені файли у стані `untracked`.
# 7.5 Почистити всі untracked файли з репозиторію, включно з ігнорованими.
Для повної очистки всіх untracked файлів, в тому числі тих, що ігноруються, використовується
команда `git clean`

```
student@virt-linux:~/lab2/hydrogen$ git clean -fdx
Removing file1.mawsadw123
Removing file2.mawsadw123
Removing file3.mawsadw123
Removing texty.txt

```
Як бачимо всі untracked файли видалені, разом з тими, що ігноруються.
### 8. Робота з reflog.
# 8.1 Переглянути лог станів гілок.
Іноді у випадках, коли працюєш над проектом через якісь певні причини, можна не завжди пушити всі свої
зміни на сервер. Тоді, якщо, наприклад, потрібно перемкнутись на тимчасову гілку для якоїсь швидкої роботи
зробили все, що необхідно і вирішили її видалити, але випадково видалили основну гілку проекту, виникає 
запитання, як все повернути.
Зазначимо, що в дійсності видаляючи гілку, ми видалили лише посилання на коміт, але самі коміти ще будуть 
деякий час в історії бази даних гіта. Тоді, якщо видалена гілка нам потрібна, або ж якась певна гілка
має не зрозумілий стан, то можна скористатись логом гілок `reflog`.

Переглядаємо лог станів гілок.
```
student@virt-linux:~/lab2/hydrogen$ git log --pretty=oneline --graph -n 15
* d44961b44f462944d7b6707d7e7628de3f467e39 (HEAD -> lab2-branch) file.cs
*   dbc22d507047fdc0f325db3447e95ee1a08bc656 1
|\  
| *   2f95c047f73452386def1760724ef9ffc7b0d7f6 (upstream/develop) Merge pull request #1685 from theGreatWhiteShark/phil-check-license-notice
| |\  
| | * e178b2cea346ba19b5e211702df9cb138db4b46a appVeyor: fail pipeline on failing license check
| | * 102383c0398f2d5ba0f4800d392030a6e02a88cd license check: fix typo
| | * 94ae57934fd0faf2949243a0523e12d324bf91ce automated license checking
| | * b35c013bda50af96e38e5e0df2f5ed2252980c7e PreferencesDialog: fix type in license notice
| | * 03e75f407a7c8f18174bd6403e08ec32e9fbacff AboutDialog: rename UI file
| |/  
* | 06f34c1b3b0c7904a8e10cf434bc1847f9677ace (origin/lab2-branch) renamed file1.h to f1.h
* | 55fe96ace12bba4bd94d01c5862ffef135c3f028 deleted file main.cpp
* |   79258e3e00d952f75acdd1f5eab15e6570ac3c7e Merge branch 'lab2-branch' of file:///home/student/Lab1/hydrogen into lab2-branch
|\ \  
| * | 1912a4ed4c9a1c16bf6765b4db83303f7e75ab77 renamed file file.json to f.json
| * | 420a39a644ebb62015048c1fbf131d3258bc8391 file main.cpp was deleted
| * |   aae0e716679f2622ca89b6cd7d9b663a056d28e7 Merge branch 'lab2-branch' of file:////home/student/Lab1/hydrogen into lab2-branch
| |\ \  
| | * | 464718138cc82e5930a57d198e8b87241021d866 textfile.txt was added

```
В цей момент локальна гілка `lab2-branch` повинна мати чимало нових комітів порівняно із її
станом на сервері origin/lab2-branch. 

Переключаємось на гілку `master` та видаляємо `lab2-branch`, зазгачимо, що переключатись обов'язково
бо не можна видаляти поточні активні гілки, а також видалимо цю гілку з серверу.

```
student@virt-linux:~/lab2/hydrogen$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.

student@virt-linux:~/lab2/hydrogen$ git branch -D lab2-branch
Deleted branch lab2-branch (was 6d14bfe6b).


student@virt-linux:~/lab2/hydrogen$ git push -d origin lab2-branch
To file:///home/student/Lab1/hydrogen
 - [deleted]             lab2-branch

```

Ніби зі всього здається, що стан цієї гілки втрачено. Але переглянемо `reflog`

```
student@virt-linux:~/lab2/hydrogen$ git reflog
e1c32b916 (HEAD -> master, origin/master, origin/HEAD) HEAD@{0}: checkout: moving from lab2-branch to master
6d14bfe6b HEAD@{1}: commit: commit n
d44961b44 HEAD@{2}: reset: moving to HEAD
d44961b44 HEAD@{3}: reset: moving to HEAD
d44961b44 HEAD@{4}: cherry-pick: file.cs
dbc22d507 HEAD@{5}: checkout: moving from lab2-branch2 to lab2-branch
f109e1ef7 (lab2-branch2) HEAD@{6}: checkout: moving from lab2-branch2 to lab2-branch2
f109e1ef7 (lab2-branch2) HEAD@{7}: commit: new.cs was added
111614ec3 HEAD@{8}: commit: file.cs
97e4cfc1e HEAD@{9}: commit: fff.txt added
dbc22d507 HEAD@{10}: checkout: moving from lab2-branch to lab2-branch2
dbc22d507 HEAD@{11}: commit (merge): 1
06f34c1b3 HEAD@{12}: commit: renamed file1.h to f1.h
55fe96ace HEAD@{13}: commit: deleted file main.cpp
79258e3e0 HEAD@{14}: pull origin lab2-branch: Merge made by the 'recursive' strategy.
a45bbb160 HEAD@{15}: commit: all files were added
4d3493899 HEAD@{16}: commit: 1 file was added
e1c32b916 (HEAD -> master, origin/master, origin/HEAD) HEAD@{17}: checkout: moving from master to lab2-branch
e1c32b916 (HEAD -> master, origin/master, origin/HEAD) HEAD@{18}: clone: from file:///home/student/Lab1/hydrogen

```

# 8.2 Створити нову гілку на будь-який стан зі списку та переключитися на цю гілку.

А тепер по рефлогу легко бачимо, що можна відновити ще нашу гілку, що ми і робимо
створючи нову гілку і одразу на неї переключаємось.

```
student@virt-linux:~/lab2/hydrogen$ git branch lab2-resurrected d44961b44
student@virt-linux:~/lab2/hydrogen$ git checkout lab2-resurrected
Switched to branch 'lab2-resurrected'
student@virt-linux:~/lab2/hydrogen$ 


student@virt-linux:~/lab2/hydrogen$ git log --pretty=oneline --graph -n 15
* d44961b44f462944d7b6707d7e7628de3f467e39 (HEAD -> lab2-resurrected) file.cs
*   dbc22d507047fdc0f325db3447e95ee1a08bc656 1
|\  
| *   2f95c047f73452386def1760724ef9ffc7b0d7f6 (upstream/develop) Merge pull request #1685 from theGreatWhiteShark/phil-check-license-notice
| |\  
| | * e178b2cea346ba19b5e211702df9cb138db4b46a appVeyor: fail pipeline on failing license check
| | * 102383c0398f2d5ba0f4800d392030a6e02a88cd license check: fix typo
| | * 94ae57934fd0faf2949243a0523e12d324bf91ce automated license checking
| | * b35c013bda50af96e38e5e0df2f5ed2252980c7e PreferencesDialog: fix type in license notice
| | * 03e75f407a7c8f18174bd6403e08ec32e9fbacff AboutDialog: rename UI file
| |/  
* | 06f34c1b3b0c7904a8e10cf434bc1847f9677ace renamed file1.h to f1.h
* | 55fe96ace12bba4bd94d01c5862ffef135c3f028 deleted file main.cpp
* |   79258e3e00d952f75acdd1f5eab15e6570ac3c7e Merge branch 'lab2-branch' of file:///home/student/Lab1/hydrogen into lab2-branch
|\ \  
| * | 1912a4ed4c9a1c16bf6765b4db83303f7e75ab77 renamed file file.json to f.json
| * | 420a39a644ebb62015048c1fbf131d3258bc8391 file main.cpp was deleted
| * |   aae0e716679f2622ca89b6cd7d9b663a056d28e7 Merge branch 'lab2-branch' of file:////home/student/Lab1/hydrogen into lab2-branch
| |\ \  
| | * | 464718138cc82e5930a57d198e8b87241021d866 textfile.txt was added

```
Бачимо, що гілка відновлена до стану, який був після чері-піку.